import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LivreDialogEditComponent } from './livre-dialog-edit.component';

describe('LivreDialogEditComponent', () => {
  let component: LivreDialogEditComponent;
  let fixture: ComponentFixture<LivreDialogEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LivreDialogEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LivreDialogEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
