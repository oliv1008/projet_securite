import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Livre } from '../../modele/livre';
import { LivresService } from '../../services/livres.service';

@Component({
  selector: 'app-livre-dialog-edit',
  templateUrl: './livre-dialog-edit.component.html',
  styleUrls: ['./livre-dialog-edit.component.scss'],
})
export class LivreDialogEditComponent implements OnInit {
  editForm = this.fb.group({
    id: [{ value: this.livre.id, disabled: true }],
    titre: [this.livre.titre],
    auteur: [this.livre.auteur],
  });

  constructor(
    private dialogRef: MatDialogRef<LivreDialogEditComponent>,
    @Inject(MAT_DIALOG_DATA) public livre: Livre,
    private fb: FormBuilder,
    private livreService: LivresService
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    //Si le serveur arrive à modifier (status 200), on ferme la boite de dialogue et on propage la réponse du serveur
    this.livreService
      .putLivre(this.editForm.getRawValue())
      .subscribe((response) => {
        if (response.status == 200) {
          this.dialogRef.close(200);
        } else {
          console.log('Erreur : ', response); //TODO : Gérer l'erreur plus proprement (affiché un message)
        }
      });
  }
}
