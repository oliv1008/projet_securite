import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LivreDialogCreateComponent } from './livre-dialog-create.component';

describe('LivreDialogCreateComponent', () => {
  let component: LivreDialogCreateComponent;
  let fixture: ComponentFixture<LivreDialogCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LivreDialogCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LivreDialogCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
