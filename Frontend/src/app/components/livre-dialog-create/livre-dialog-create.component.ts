import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { LivresService } from 'src/app/services/livres.service';

@Component({
  selector: 'app-livre-dialog-create',
  templateUrl: './livre-dialog-create.component.html',
  styleUrls: ['./livre-dialog-create.component.scss'],
})
export class LivreDialogCreateComponent implements OnInit {
  createForm = this.fb.group({
    titre: [''],
    auteur: [''],
  });

  constructor(
    private dialogRef: MatDialogRef<LivreDialogCreateComponent>,
    private fb: FormBuilder,
    private livreService: LivresService
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    //Si le serveur arrive à ajouter (status 201), on ferme la boite de dialogue et on propage la réponse du serveur
    this.livreService.postLivre(this.createForm.value).subscribe((response) => {
      if (response.status == 201) {
        this.dialogRef.close(201);
      } else {
        console.log('Erreur : ', response); //TODO : Gérer l'erreur plus proprement (affiché un message)
      }
    });
  }
}
