import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Livre } from 'src/app/modele/livre';
import { LivresService } from 'src/app/services/livres.service';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss'],
})
export class DialogConfirmComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<DialogConfirmComponent>,
    private livreService: LivresService,
    @Inject(MAT_DIALOG_DATA) public livre: Livre
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    //Si le serveur arrive à supprimer (status 204), on ferme la boite de dialogue et on propage la réponse du serveur
    this.livreService.deleteLivre(this.livre).subscribe((response) => {
      if (response.status == 204) {
        this.dialogRef.close(204);
      } else {
        console.log('Erreur : ', response); //TODO : Gérer l'erreur plus proprement (affiché un message)
      }
    });
  }
}
