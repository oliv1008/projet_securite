import { Component, OnInit } from '@angular/core';
import { LivresService } from '../../services/livres.service';
import { Livre } from '../../modele/livre';
import { MatDialog } from '@angular/material/dialog';
import { LivreDialogEditComponent } from '../livre-dialog-edit/livre-dialog-edit.component';
import { LivreDialogCreateComponent } from '../livre-dialog-create/livre-dialog-create.component';
import { DialogConfirmComponent } from '../dialog-confirm/dialog-confirm.component';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss'],
})
export class CrudComponent implements OnInit {
  displayedColumns: string[] = ['id', 'auteur', 'titre', 'actions'];
  livres: Livre[] = [];
  userRoles: string[] = [];

  constructor(
    private livreService: LivresService,
    public dialog: MatDialog,
    private readonly keycloak: KeycloakService
  ) {}

  public ngOnInit() {
    this.getAllLivres();
    this.userRoles = this.keycloak.getUserRoles(); // On charge les rôles de l'utilisateur pour savoir quoi afficher sur la page de crud
  }

  /**
   *  Charge la liste de tout les livres et en remplie la source de données pour le tableau
   */
  getAllLivres() {
    return this.livreService.getAllLivres().subscribe(
      (data) => {
        this.livres = data;
      },
      (error: any) => {
        console.log(error);
      },
      () => {
        console.log('On a récupéré la liste de tout les livres');
      }
    );
  }

  /**
   * Ouvre une boite de dialogue pour éditer un livre.
   * @param livre Le livre qui sera édité
   */
  onEditClick(livre: Livre) {
    this.dialog
      .open(LivreDialogEditComponent, {
        data: {
          id: livre.id,
          titre: livre.titre,
          auteur: livre.auteur,
        },
      })
      // Si l'édition c'est bien réalisée, on recharge la liste des livres pour actualiser l'affichage
      .afterClosed()
      .subscribe((data) => {
        if (data == 200) {
          this.getAllLivres(); // TODO : Recharger uniquement le livre modifié et pas tout les livres
        }
      });
  }

  /**
   * Ouvre une boite de dialogue qui demande confirmation pour la suppression du livrE.
   * @param livre Le livre qui sera supprimé
   */
  onDeleteClick(livre: Livre) {
    this.dialog
      .open(DialogConfirmComponent, {
        data: {
          id: livre.id,
          titre: livre.titre,
          auteur: livre.auteur,
        },
      })
      // Si la suppression c'est bien réalisée, on recharge la liste des livres pour actualiser l'affichage
      .afterClosed()
      .subscribe((data) => {
        if (data == 204) {
          this.getAllLivres(); // TODO : Recharger uniquement le livre modifié et pas tout les livres
        }
      });
  }

  /**
   * Ouvre une boite de dialogue pour créer un livre
   */
  onAddClick() {
    this.dialog
      .open(LivreDialogCreateComponent)
      // Si l'ajout c'est bien réalisé, on recharge la liste des livres pour actualiser l'affichage
      .afterClosed()
      .subscribe((data) => {
        if (data == 201) {
          this.getAllLivres(); // TODO : Recharger uniquement le livre modifié et pas tout les livres
        }
      });
  }
}
