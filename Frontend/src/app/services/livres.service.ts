import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Livre } from '../modele/livre';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LivresService {
  private livresUrl: string = 'http://127.0.0.1:8000/livres/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    observe: 'response',
  };

  constructor(private http: HttpClient) {}

  /** GET all */
  getAllLivres(): Observable<Livre[]> {
    return this.http.get<Livre[]>(this.livresUrl);
  }

  /** GET one by id */
  getLivreById(livre: number | Livre): Observable<Livre> {
    const id = typeof livre === 'number' ? livre : livre.id;
    const url = `${this.livresUrl}${id}/`;

    return this.http.get<Livre>(url);
  }

  /** Bug incompréhensible pour les méthodes POST, PUT et DELETE : impossible de mettre les options dans une variable sinon erreur de compilation */
  /** POST one */
  postLivre(livre: Livre): Observable<HttpResponse<Livre>> {
    return this.http.post<Livre>(this.livresUrl, livre, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'response',
    });
  }

  /** Bug incompréhensible pour les méthodes POST, PUT et DELETE : impossible de mettre les options dans une variable sinon erreur de compilation */
  /** PUT one */
  putLivre(livre: Livre): Observable<HttpResponse<Livre>> {
    const id = livre.id;
    const url = `${this.livresUrl}${id}/`;

    return this.http.put<Livre>(url, livre, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'response',
    });
  }

  /** Bug incompréhensible pour les méthodes POST, PUT et DELETE : impossible de mettre les options dans une variable sinon erreur de compilation */
  /** DELETE one */
  deleteLivre(livre: number | Livre): Observable<HttpResponse<Livre>> {
    const id = typeof livre === 'number' ? livre : livre.id;
    const url = `${this.livresUrl}${id}/`;

    return this.http.delete<Livre>(url, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'response',
    });
  }
}
