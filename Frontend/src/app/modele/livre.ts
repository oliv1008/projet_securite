export interface Livre {
  id: number;
  auteur: string;
  titre: string;
}
