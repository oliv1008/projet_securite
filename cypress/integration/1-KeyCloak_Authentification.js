describe('KeyCloak_Authentification', function() {
    it('sucessfull authent admin', function() {
        cy.visit('http://localhost:4200/')
		cy.contains('User not logged in')
		cy.contains('login').click()
		cy.get('#username').type("admin")
		cy.get('#password').type("admin")
		cy.get('#kc-form-login').submit()
		cy.url().should('eq', 'http://localhost:4200/dashboard')
		cy.contains('admin')
    })
	
	it('sucessfull authent user', function() {
        cy.visit('http://localhost:4200/')
		cy.contains('User not logged in')
		cy.contains('login').click()
		cy.get('#username').type("user")
		cy.get('#password').type("user")
		cy.get('#kc-form-login').submit()
		cy.url().should('eq', 'http://localhost:4200/dashboard')
		cy.contains('user')
    })
	
	it('bad username', function() {
        cy.visit('http://localhost:4200/')
		cy.contains('User not logged in')
		cy.contains('login').click()
		cy.get('#username').type("noUser")
		cy.get('#password').type("noPassword")
		cy.get('#kc-form-login').submit()
		cy.contains('Invalid username or password.')
    })
	
	it('bad password', function() {
        cy.visit('http://localhost:4200/')
		cy.contains('User not logged in')
		cy.contains('login').click()
		cy.get('#username').type("admin")
		cy.get('#password').type("noPassword")
		cy.get('#kc-form-login').submit()
		cy.contains('Invalid username or password.')
    })

	it('no user typed', function() {
        cy.visit('http://localhost:4200/')
		cy.contains('User not logged in')
		cy.contains('login').click()
		cy.get('#password').type("noPassword")
		cy.get('#kc-form-login').submit()
		cy.contains('Invalid username or password.')
    })

	it('no password typed', function() {
        cy.visit('http://localhost:4200/')
		cy.contains('User not logged in')
		cy.contains('login').click()
		cy.get('#username').type("admin")
		cy.get('#kc-form-login').submit()
		cy.contains('Invalid username or password.')
    })

	it('nothing typed', function() {
        cy.visit('http://localhost:4200/')
		cy.contains('User not logged in')
		cy.contains('login').click()
		cy.get('#username').type("admin")
		cy.get('#password').type("noPassword")
		cy.get('#kc-form-login').submit()
		cy.contains('Invalid username or password.')
    })
})
