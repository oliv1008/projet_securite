describe('UpdateBook', function() {
	it('admin can delete', function() {
        loginAdmin()
        cy.contains('CRUD page').click()
        cy.url().should('eq', 'http://localhost:4200/crud')
        cy.get('[id="edit-1"]').click()
        cy.get('#mat-input-1').type("testTitreEdition")
		cy.get('#mat-input-2').type("testAuteurEdition")
        cy.contains('Ok').click()
		cy.contains("testTitreEdition")
		cy.contains("testAuteurEdition")
    })
	
	it('user can delete', function() {
        loginUser()
        cy.contains('CRUD page').click()
        cy.url().should('eq', 'http://localhost:4200/crud')
        cy.get('[id="edit-1"]').click()
        cy.get('#mat-input-1').type("testTitreEdition")
		cy.get('#mat-input-2').type("testAuteurEdition")
        cy.contains('Ok').click()
		cy.contains("testTitreEdition")
		cy.contains("testAuteurEdition")
    })
	

    function loginAdmin (){
		cy.visit('http://localhost:4200/')
		cy.contains('login').click()
		cy.get('#username').type("admin")
		cy.get('#password').type("admin")
		cy.get('#kc-form-login').submit()
	}
	
	function loginUser (){
		cy.visit('http://localhost:4200/')
		cy.contains('login').click()
		cy.get('#username').type("user")
		cy.get('#password').type("user")
		cy.get('#kc-form-login').submit()
	}
})
