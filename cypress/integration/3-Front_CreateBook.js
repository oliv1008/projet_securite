describe('CreateBook', function() {
	it('admin can create', function() {
        loginAdmin()
		cy.contains('CRUD page').click()
		cy.url().should('eq', 'http://localhost:4200/crud')
		cy.contains('Ajouter un livre').click()
		cy.get('#mat-input-0').type("testTitreAjout")
		cy.get('#mat-input-1').type("testAuteurAjout")
		cy.contains('Ok').click()
		cy.contains("testTitreAjout")
		cy.contains("testAuteurAjout")
    })
	
	it('user can create', function() {
        loginUser()
		cy.contains('CRUD page').click()
		cy.url().should('eq', 'http://localhost:4200/crud')
		cy.contains('Ajouter un livre').click()
		cy.get('#mat-input-0').type("testTitreAjout")
		cy.get('#mat-input-1').type("testAuteurAjout")
		cy.contains('Ok').click()
		cy.contains("testTitreAjout")
		cy.contains("testAuteurAjout")
    })

	
	function loginAdmin (){
		cy.visit('http://localhost:4200/')
		cy.contains('login').click()
		cy.get('#username').type("admin")
		cy.get('#password').type("admin")
		cy.get('#kc-form-login').submit()
	}
	
	function loginUser (){
		cy.visit('http://localhost:4200/')
		cy.contains('login').click()
		cy.get('#username').type("user")
		cy.get('#password').type("user")
		cy.get('#kc-form-login').submit()
	}
})
