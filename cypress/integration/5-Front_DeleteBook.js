describe('DeleteBook', function() {
    it('admin can delete', function() {
        loginAdmin()
		cy.contains('CRUD page').click()
		cy.url().should('eq', 'http://localhost:4200/crud')
		cy.contains('1')
		cy.get('[id="delete-1"]').click()
		cy.contains('Yes').click()
		cy.contains('1').should('not.exist')
    })
	
	it('user cant delete', function() {
        loginUser()
		cy.contains('CRUD page').click()
		cy.url().should('eq', 'http://localhost:4200/crud')
		cy.get('[id="delete-*"]').should('not.exist')
    })

	
	function loginAdmin (){
		cy.visit('http://localhost:4200/')
		cy.contains('login').click()
		cy.get('#username').type("admin")
		cy.get('#password').type("admin")
		cy.get('#kc-form-login').submit()
	}
	
	function loginUser (){
		cy.visit('http://localhost:4200/')
		cy.contains('login').click()
		cy.get('#username').type("user")
		cy.get('#password').type("user")
		cy.get('#kc-form-login').submit()
	}
})
