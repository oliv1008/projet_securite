describe('ReadBook', function() {
    it('admin can read', function() {
        loginAdmin()
        cy.contains('CRUD page').click()
        cy.url().should('eq', 'http://localhost:4200/crud')
        cy.contains("test")
        cy.contains("test2")
    })
	
	it('user can read', function() {
        loginUser()
        cy.contains('CRUD page').click()
        cy.url().should('eq', 'http://localhost:4200/crud')
        cy.contains("test")
        cy.contains("test2")
    })


	function loginAdmin (){
		cy.visit('http://localhost:4200/')
		cy.contains('login').click()
		cy.get('#username').type("admin")
		cy.get('#password').type("admin")
		cy.get('#kc-form-login').submit()
	}
	
	function loginUser (){
		cy.visit('http://localhost:4200/')
		cy.contains('login').click()
		cy.get('#username').type("user")
		cy.get('#password').type("user")
		cy.get('#kc-form-login').submit()
	}
})


