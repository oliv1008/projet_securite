from django.shortcuts import render
from rest_framework import viewsets

from .serializers import LivreSerializer
from .models import Livre

# Create your views here.

class LivreViewSet(viewsets.ModelViewSet):
    queryset = Livre.objects.all().order_by('titre')
    serializer_class = LivreSerializer