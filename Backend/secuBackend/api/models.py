from django.db import models

class Livre(models.Model):
    titre = models.CharField(max_length=60)
    auteur = models.CharField(max_length=60)

    def __str__(self):
        return self.titre